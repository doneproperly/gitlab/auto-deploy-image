#!/bin/bash -e

[[ "$TRACE" ]] && set -x

export TILLER_NAMESPACE=$KUBE_NAMESPACE
export HELM_HOST="localhost:44134"
export AUTO_DEPLOY_ENVIRONMENT_VALUES_FILE=/tmp/auto-deploy-environment-values.yaml
export RELEASE_NAME=${HELM_RELEASE_NAME:-$CI_ENVIRONMENT_SLUG}
export POSTGRESQL_RELEASE_NAME="${RELEASE_NAME}-postgresql"
export AUTO_DEVOPS_POSTGRES_CHANNEL=${AUTO_DEVOPS_POSTGRES_CHANNEL:-"2"}
if [[ "$AUTO_DEVOPS_POSTGRES_CHANNEL" == "2" ]]; then
  export POSTGRES_VERSION="${POSTGRES_VERSION:-"9.6.16"}"
elif [[ "$AUTO_DEVOPS_POSTGRES_CHANNEL" == "1" ]]; then
  export POSTGRES_VERSION="${POSTGRES_VERSION:-"9.6.2"}"
fi

function check_kube_domain() {
  if [[ -z "$KUBE_INGRESS_BASE_DOMAIN" ]]; then
    echo "In order to deploy or use Review Apps,"
    echo "KUBE_INGRESS_BASE_DOMAIN variables must be set"
    echo "From 11.8, you can set KUBE_INGRESS_BASE_DOMAIN in cluster settings"
    echo "or by defining a variable at group or project level."
    echo "You can also manually add it in .gitlab-ci.yml"
    false
  else
    true
  fi
}

function download_chart() {
  local auto_chart
  local auto_chart_name
  if [[ ! -d chart ]]; then
    auto_chart=${AUTO_DEVOPS_CHART:-gitlab/auto-deploy-app}
    # shellcheck disable=SC2086 # double quote variables to prevent globbing
    auto_chart_name=$(basename $auto_chart)
    auto_chart_name=${auto_chart_name%.tgz}
    auto_chart_name=${auto_chart_name%.tar.gz}
  else
    auto_chart="chart"
    auto_chart_name="chart"
  fi

  helm init --client-only
  # shellcheck disable=SC2086 # double quote variables to prevent globbing
  # shellcheck disable=SC2140 # ambiguous quoting warning
  helm repo add ${AUTO_DEVOPS_CHART_REPOSITORY_NAME:-gitlab} ${AUTO_DEVOPS_CHART_REPOSITORY:-https://charts.gitlab.io} ${AUTO_DEVOPS_CHART_REPOSITORY_USERNAME:+"--username" "$AUTO_DEVOPS_CHART_REPOSITORY_USERNAME"} ${AUTO_DEVOPS_CHART_REPOSITORY_PASSWORD:+"--password" "$AUTO_DEVOPS_CHART_REPOSITORY_PASSWORD"}
  if [[ ! -d "$auto_chart" ]]; then
    helm fetch ${auto_chart} --untar
  fi
  if [ "$auto_chart_name" != "chart" ]; then
    mv ${auto_chart_name} chart
  fi

  helm dependency update chart/
  helm dependency build chart/
}

function ensure_namespace() {
  kubectl get namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"
}

function initialize_tiller() {
  echo "Checking Tiller..."

  nohup tiller -listen ${HELM_HOST} -alsologtostderr >/dev/null 2>&1 &
  echo "Tiller is listening on ${HELM_HOST}"

  if ! helm version --debug; then
    echo "Failed to init Tiller."
    return 1
  fi
  echo ""
}

function write_environment_values_file() {
  echo "deploymentApiVersion: apps/v1" >"$AUTO_DEPLOY_ENVIRONMENT_VALUES_FILE"
}

function create_secret() {
  echo "Create secret..."
  if [[ "$CI_PROJECT_VISIBILITY" == "public" ]]; then
    return
  fi

  kubectl create secret -n "$KUBE_NAMESPACE" \
    docker-registry "gitlab-registry-${CI_PROJECT_PATH_SLUG}" \
    --docker-server="$CI_REGISTRY" \
    --docker-username="${CI_DEPLOY_USER:-$CI_REGISTRY_USER}" \
    --docker-password="${CI_DEPLOY_PASSWORD:-$CI_REGISTRY_PASSWORD}" \
    --docker-email="$GITLAB_USER_EMAIL" \
    -o yaml --dry-run | kubectl replace -n "$KUBE_NAMESPACE" --force -f -
}

# shellcheck disable=SC2086
function persist_environment_url() {
  echo $CI_ENVIRONMENT_URL >environment_url.txt
}

function auto_database_url() {
  local auto_database_url
  if [[ "$AUTO_DEVOPS_POSTGRES_CHANNEL" == "2" ]]; then
    auto_database_url="postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRESQL_RELEASE_NAME}:5432/${POSTGRES_DB}"
  elif [[ "$AUTO_DEVOPS_POSTGRES_CHANNEL" == "1" ]]; then
    auto_database_url="postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${RELEASE_NAME}-postgres:5432/${POSTGRES_DB}"
  else
    echo "Un-recognized \$AUTO_DEVOPS_POSTGRES_CHANNEL variable '${AUTO_DEVOPS_POSTGRES_CHANNEL}'. Accepted values are '1' and '2'"

    exit 1
  fi

  echo "${DATABASE_URL-$auto_database_url}"
}

function install_postgresql() {
  if [[ "$AUTO_DEVOPS_POSTGRES_CHANNEL" != "2" ]]; then
    echo "Separate postgres chart is only supported for AUTO_DEVOPS_POSTGRES_CHANNEL:2"

    exit 1
  fi

  if [[ "$POSTGRES_VERSION" == "9.6.2" ]]; then
    echo "The minimum supported image tag for AUTO_DEVOPS_POSTGRES_CHANNEL:2 is 9.6.16"

    exit 1
  fi

  local name="$POSTGRESQL_RELEASE_NAME"

  helm upgrade --install \
    --atomic \
    --wait \
    --version 8.2.1 \
    --set fullnameOverride="$name" \
    --set postgresqlUsername="$POSTGRES_USER" \
    --set postgresqlPassword="$POSTGRES_PASSWORD" \
    --set postgresqlDatabase="$POSTGRES_DB" \
    --set image.tag="$POSTGRES_VERSION" \
    --namespace="$KUBE_NAMESPACE" \
    "$name" \
    stable/postgresql
}

# shellcheck disable=SC2153 # warns that my_var vs MY_VAR is a possible misspelling
# shellcheck disable=SC2154 # env_ADDITIONAL_HOSTS eval assignment is not recognized
function deploy() {
  local track="${1-stable}"
  local percentage="${2:-100}"

  local name
  name=$(deploy_name "$track")

  local stable_name
  stable_name=$(deploy_name stable)

  local old_postgres_already_enabled
  local old_postgres_enabled
  if [[ "$POSTGRES_ENABLED" == "true" && "$AUTO_DEVOPS_POSTGRES_CHANNEL" == "2" ]]; then
    old_postgres_already_enabled=$( (helm get values --output json "$stable_name" || echo '{}') | jq '.postgresql.enabled')
    if [[ -z "$AUTO_DEVOPS_POSTGRES_DELETE_V1" ]] && [[ "$old_postgres_already_enabled" == "true" ]]; then
      echo 'Detected an existing PostgreSQL database installed on the
deprecated channel 1, but the current channel is set to 2. The default
channel changed to 2 in of GitLab 13.0.

- To continue using the channel 1 PostgreSQL database, set
  AUTO_DEVOPS_POSTGRES_CHANNEL to 1 and redeploy

- OR, to proceed with deleting the channel 1 PostgreSQL database
  and install a fresh channel 2 database, set AUTO_DEVOPS_POSTGRES_DELETE_V1
  to a non-empty value and redeploy.

  WARNING: This will PERMANENTLY DELETE the existing channel 1 database.

  For details on backing up your database and upgrading channels, see
  https://docs.gitlab.com/ee/topics/autodevops/upgrading_postgresql.html'

      exit 1
    fi

    install_postgresql

    # Do not install the older 0.7.1 postgresql sub-chart
    old_postgres_enabled="false"
  else
    old_postgres_enabled="$POSTGRES_ENABLED"
  fi

  local database_url
  database_url=$(auto_database_url)

  local image_repository
  local image_tag

  if [[ -z "$CI_COMMIT_TAG" ]]; then
    image_repository=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG}
    image_tag=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA}
  else
    image_repository=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE}
    image_tag=${CI_APPLICATION_TAG:-$CI_COMMIT_TAG}
  fi

  local old_postgres_enabled_for_track="$old_postgres_enabled"
  local postgres_managed="$AUTO_DEVOPS_POSTGRES_MANAGED"
  local postgres_managed_selector="$AUTO_DEVOPS_POSTGRES_MANAGED_CLASS_SELECTOR"

  # if track is different than stable,
  # re-use all attached resources
  if [[ "$track" != "stable" ]]; then
    old_postgres_enabled_for_track="false"
  fi

  local replicas
  replicas=$(get_replicas "$track" "$percentage")

  local secret_name
  if [[ "$CI_PROJECT_VISIBILITY" != "public" ]]; then
    secret_name="gitlab-registry-${CI_PROJECT_PATH_SLUG}"
  else
    secret_name=''
  fi

  local modsecurity_set_args=()
  if [[ -n "$AUTO_DEVOPS_MODSECURITY_SEC_RULE_ENGINE" ]]; then
    modsecurity_set_args=("--set" "ingress.modSecurity.enabled=true,ingress.modSecurity.secRuleEngine=$AUTO_DEVOPS_MODSECURITY_SEC_RULE_ENGINE")
  fi

  create_application_secret "$track"

  local env_slug
  env_slug=$(echo "${CI_ENVIRONMENT_SLUG//-/_}" | tr '[:lower:]' '[:upper:]')

  local additional_hosts
  eval local env_ADDITIONAL_HOSTS="\$${env_slug}_ADDITIONAL_HOSTS"
  if [ -n "$env_ADDITIONAL_HOSTS" ]; then
    additional_hosts="{$env_ADDITIONAL_HOSTS}"
  elif [ -n "$ADDITIONAL_HOSTS" ]; then
    additional_hosts="{$ADDITIONAL_HOSTS}"
  fi

  local helm_values_args=()
  local helm_values_file=${HELM_UPGRADE_VALUES_FILE:-.gitlab/auto-deploy-values.yaml}
  if [[ -f "${helm_values_file}" ]]; then
    echo "Using helm values file ${helm_values_file@Q}"
    helm_values_args=(--values "${helm_values_file}")
  else
    echo "No helm values file found at ${helm_values_file@Q}"
  fi

  local atomic_flag=()
  if [[ "$AUTO_DEVOPS_ATOMIC_RELEASE" != "false" ]]; then
    atomic_flag=('--atomic')
  fi

  # TODO: Over time, migrate all --set values to this file, see https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/-/issues/31
  write_environment_values_file

  if [[ -n "$DB_INITIALIZE" && -z "$(helm ls -q "^$stable_name$")" ]]; then
    echo "Initializing service URL and database. No deployment will be created"
    # shellcheck disable=SC2086 # HELM_UPGRADE_EXTRA_ARGS -- double quote variables to prevent globbing
    helm upgrade --install \
      "${atomic_flag[@]}" \
      --wait \
      --set gitlab.app="$CI_PROJECT_PATH_SLUG" \
      --set gitlab.env="$CI_ENVIRONMENT_SLUG" \
      --set gitlab.envName="$CI_ENVIRONMENT_NAME" \
      --set gitlab.envURL="$CI_ENVIRONMENT_URL" \
      --set releaseOverride="$RELEASE_NAME" \
      --set image.repository="$image_repository" \
      --set image.tag="$image_tag" \
      --set image.secrets[0].name="$secret_name" \
      --set application.track="stable" \
      --set application.database_url="$database_url" \
      --set application.secretName="$APPLICATION_SECRET_NAME" \
      --set application.secretChecksum="$APPLICATION_SECRET_CHECKSUM" \
      --set service.commonName="le-$CI_PROJECT_ID.$KUBE_INGRESS_BASE_DOMAIN" \
      --set service.url="$CI_ENVIRONMENT_URL" \
      --set service.additionalHosts="$additional_hosts" \
      --set replicaCount="$replicas" \
      --set postgresql.enabled="$old_postgres_enabled" \
      --set postgresql.managed="$postgres_managed" \
      --set postgresql.managedClassSelector="$postgres_managed_selector" \
      --set postgresql.nameOverride="postgres" \
      --set postgresql.postgresUser="$POSTGRES_USER" \
      --set postgresql.postgresPassword="$POSTGRES_PASSWORD" \
      --set postgresql.postgresDatabase="$POSTGRES_DB" \
      --set postgresql.imageTag="$POSTGRES_VERSION" \
      --set application.initializeCommand="$DB_INITIALIZE" \
      "${modsecurity_set_args[@]}" \
      --values "$AUTO_DEPLOY_ENVIRONMENT_VALUES_FILE" \
      "${helm_values_args[@]}" \
      $HELM_UPGRADE_EXTRA_ARGS \
      --namespace="$KUBE_NAMESPACE" \
      "$stable_name" \
      chart/
  fi

  echo "Deploying new $track release..."
  # shellcheck disable=SC2086 # HELM_UPGRADE_EXTRA_ARGS -- double quote variables to prevent globbing
  helm upgrade --install \
    "${atomic_flag[@]}" \
    --wait \
    --set gitlab.app="$CI_PROJECT_PATH_SLUG" \
    --set gitlab.env="$CI_ENVIRONMENT_SLUG" \
    --set gitlab.envName="$CI_ENVIRONMENT_NAME" \
    --set gitlab.envURL="$CI_ENVIRONMENT_URL" \
    --set releaseOverride="$RELEASE_NAME" \
    --set image.repository="$image_repository" \
    --set image.tag="$image_tag" \
    --set image.secrets[0].name="$secret_name" \
    --set application.track="$track" \
    --set application.database_url="$database_url" \
    --set application.secretName="$APPLICATION_SECRET_NAME" \
    --set application.secretChecksum="$APPLICATION_SECRET_CHECKSUM" \
    --set service.commonName="le-$CI_PROJECT_ID.$KUBE_INGRESS_BASE_DOMAIN" \
    --set service.url="$CI_ENVIRONMENT_URL" \
    --set service.additionalHosts="$additional_hosts" \
    --set replicaCount="$replicas" \
    --set postgresql.enabled="$old_postgres_enabled_for_track" \
    --set postgresql.managed="$postgres_managed" \
    --set postgresql.managedClassSelector="$postgres_managed_selector" \
    --set postgresql.nameOverride="postgres" \
    --set postgresql.postgresUser="$POSTGRES_USER" \
    --set postgresql.postgresPassword="$POSTGRES_PASSWORD" \
    --set postgresql.postgresDatabase="$POSTGRES_DB" \
    --set postgresql.imageTag="$POSTGRES_VERSION" \
    --set application.initializeCommand="" \
    --set application.migrateCommand="$DB_MIGRATE" \
    "${modsecurity_set_args[@]}" \
    --values "$AUTO_DEPLOY_ENVIRONMENT_VALUES_FILE" \
    "${helm_values_args[@]}" \
    $HELM_UPGRADE_EXTRA_ARGS \
    --namespace="$KUBE_NAMESPACE" \
    "$name" \
    chart/

  if [[ -z "$ROLLOUT_STATUS_DISABLED" ]]; then
    kubectl rollout status -n "$KUBE_NAMESPACE" -w "$ROLLOUT_RESOURCE_TYPE/$name"
  fi
}

function scale() {
  local track="${1-stable}"
  local percentage="${2-100}"
  local name
  name=$(deploy_name "$track")

  local replicas
  replicas=$(get_replicas "$track" "$percentage")

  if [[ -n "$(helm ls -q "^$name$")" ]]; then
    helm upgrade --reuse-values \
      --wait \
      --set replicaCount="$replicas" \
      --namespace="$KUBE_NAMESPACE" \
      "$name" \
      chart/
  fi
}

function delete_postgresql() {
  local name="$POSTGRESQL_RELEASE_NAME"

  if [[ -n "$(helm ls -q "^$name$")" ]]; then
    helm delete --purge "$name"
  fi
}

function delete() {
  local track="${1-stable}"
  local name
  name=$(deploy_name "$track")

  if [[ -n "$(helm ls -q "^$name$")" ]]; then
    helm delete --purge "$name"
  fi

  if [[ "$track" == "stable" ]]; then
    delete_postgresql
  fi

  local secret_name
  secret_name=$(application_secret_name "$track")

  kubectl delete secret --ignore-not-found -n "$KUBE_NAMESPACE" "$secret_name"
}

## Helper functions
##

# Extracts variables prefixed with K8S_SECRET_
# and creates a Kubernetes secret.
#
# e.g. If we have the following environment variables:
#   K8S_SECRET_A=value1
#   K8S_SECRET_B=multi\ word\ value
#
# Then we will create a secret with the following key-value pairs:
#   data:
#     A: dmFsdWUxCg==
#     B: bXVsdGkgd29yZCB2YWx1ZQo=
#
function create_application_secret() {
  local track="${1-stable}"

  # shellcheck disable=SC2155 # declare and assign separately to avoid masking return values.
  export APPLICATION_SECRET_NAME=$(application_secret_name "$track")

  env | sed -n "s/^K8S_SECRET_\(.*\)$/\1/p" >k8s_prefixed_variables

  kubectl create secret \
    -n "$KUBE_NAMESPACE" generic "$APPLICATION_SECRET_NAME" \
    --from-env-file k8s_prefixed_variables -o yaml --dry-run |
    kubectl replace -n "$KUBE_NAMESPACE" --force -f -

  # shellcheck disable=SC2002 # useless cat, prefer cmd < file
  # shellcheck disable=SC2155 # declare and assign separately to avoid masking return values.
  export APPLICATION_SECRET_CHECKSUM=$(cat k8s_prefixed_variables | sha256sum | cut -d ' ' -f 1)

  rm k8s_prefixed_variables
}

function application_secret_name() {
  local track="${1-stable}"
  local name
  name=$(deploy_name "$track")

  echo "${name}-secret"
}

# shellcheck disable=SC2086
function deploy_name() {
  local name="$RELEASE_NAME"
  local track="${1-stable}"

  if [[ "$track" != "stable" ]]; then
    name="$name-$track"
  fi

  echo $name
}

# shellcheck disable=SC2086 # double quote to prevent globbing
# shellcheck disable=SC2153 # incorrectly thinks replicas vs REPLICAS is a misspelling
function get_replicas() {
  local track="${1:-stable}"
  local percentage="${2:-100}"

  local env_track
  env_track=$(echo $track | tr '[:lower:]' '[:upper:]')

  local env_slug
  env_slug=$(echo ${CI_ENVIRONMENT_SLUG//-/_} | tr '[:lower:]' '[:upper:]')

  local new_replicas
  if [[ "$track" == "stable" ]] || [[ "$track" == "rollout" ]]; then
    # for stable track get number of replicas from `PRODUCTION_REPLICAS`
    eval new_replicas=\$${env_slug}_REPLICAS
    if [[ -z "$new_replicas" ]]; then
      new_replicas=$REPLICAS
    fi
  else
    # for all tracks get number of replicas from `CANARY_PRODUCTION_REPLICAS`
    eval new_replicas=\$${env_track}_${env_slug}_REPLICAS
    if [[ -z "$new_replicas" ]]; then
      eval new_replicas=\$${env_track}_REPLICAS
    fi
  fi

  local replicas="${new_replicas:-1}"
  replicas="$((replicas * percentage / 100))"

  if [[ $new_replicas == 0 ]]; then
    # If zero replicas requested, then return 0
    echo "$new_replicas"
  elif [[ $replicas -gt 0 ]]; then
    echo "$replicas"
  else
    # Return one if calculated replicas is zero
    # E.g. 25% of 2 replicas is 0 (integer division)
    echo 1
  fi
}

##
## End Helper functions

option=$1
case $option in

  check_kube_domain) check_kube_domain ;;
  download_chart) download_chart ;;
  ensure_namespace) ensure_namespace ;;
  initialize_tiller) initialize_tiller ;;
  create_secret) create_secret ;;
  persist_environment_url) persist_environment_url ;;
  auto_database_url) auto_database_url ;;
  install_postgresql) install_postgresql "${@:2}" ;;
  deploy) deploy "${@:2}" ;;
  scale) scale "${@:2}" ;;
  delete) delete "${@:2}" ;;
  create_application_secret) create_application_secret "${@:2}" ;;
  deploy_name) deploy_name "${@:2}" ;;
  get_replicas) get_replicas "${@:2}" ;;
  *) exit 1 ;;
esac
